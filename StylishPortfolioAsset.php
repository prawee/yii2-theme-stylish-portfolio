<?php
/*
* 2014-10-23
* @author Prawee Wongsa <konkeanweb@gmail.com>
* http://startbootstrap.com/template-overviews/stylish-portfolio/
*/
namespace prawee\theme\stylishportfolio;

use yii\web\AssetBundle;
class StylishPortfolioAsset extends AssetBundle
{
    public $sourcePath='@vendor/prawee/yii2-theme-stylish-portfolio/assets';
    public $baseUrl = '@web';
    
    public $css=[
        'css/stylish-portfolio.css',
        'font-awesome-4.1.0/css/font-awesome.min.css',
    ];
    
    public $js=[
        'js/jquery-1.11.0.js',
        'js/stylish-portfolio.js',
    ];
    
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init() {
        parent::init();
    }
}
