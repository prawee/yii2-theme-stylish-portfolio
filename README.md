Yii2  stylish portfolio theme 
==============================
theme for Yii2 applicaiton

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist prawee/yii2-theme-stylish-portfolio "*"
```

or add

```
"prawee/yii2-theme-stylish-portfolio": "*"
```

to the require section of your `composer.json` file.


Usage
-----
